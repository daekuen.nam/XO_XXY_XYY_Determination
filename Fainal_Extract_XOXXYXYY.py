import sys
inFile = open(sys.argv[1], 'r')
outFile = open(sys.argv[2], 'w')
Linput = inFile.readlines()
inFile.close()
#---------------------------------- trimming --------------------------------------------
# separate raw data
Lraw = []
for i in Linput :
	Lraw.append(i.split('\t'))
# remove head
Ldata = []
for i,c in enumerate(Lraw) :
	if c[0] == 'Name' :
		Ldata = Lraw[i+1:]
# remove Indel
Ldata_RID = []
for i in Ldata :
	if not i[3] in ['II', 'DD', 'ID', 'DI'] :
		Ldata_RID.append(i)
# extract X,Y
L_X_ID = []
L_Y_ID = []
for i in Ldata_RID :
	if i[1] in 'X' :
		L_X_ID.append(i) 
	if i[1] in 'Y' :
		L_Y_ID.append(i)
# extract NoCall
I_X_NoCall = 0
I_Y_NoCall = 0
for i in L_Y_ID :
	if i[3] == '..' :
		I_Y_NoCall += 1
for i in L_X_ID :
	if i[3] == '..' :
		I_X_NoCall += 1
# remove NoCall, indel
Ldata_REID = []
for i in Ldata :
	if not i[3] in ['..', 'II', 'DD', 'ID', 'DI'] :
		Ldata_REID.append(i)
# extract X,Y
L_X_EID = []
L_Y_EID = []
for i in Ldata_REID :
	if i[1] in 'X' :
		L_X_EID.append(i) 
	if i[1] in 'Y' :
		L_Y_EID.append(i)
# extract homo, hetero in chrX
I_X_hetero = 0
I_X_homo = 0
for i in L_X_EID :
	GT = list(i[3])
	if GT[0] == GT[1].strip() :
		I_X_homo += 1
	else :
		I_X_hetero += 1
# extract homo, hetero in chrY
I_Y_hetero = 0
I_Y_homo = 0
for i in L_Y_EID :
	GT = list(i[3])
	if GT[0] == GT[1].strip() :
		I_Y_homo += 1
	else :
		I_Y_hetero += 1
#---------------------------------- trimming --------------------------------------------
X_homo_ratio = float(I_X_homo)/float(len(L_X_ID))
X_hetero_ratio = float(I_X_hetero)/float(len(L_X_ID))
X_NoCall_ratio = float(I_X_NoCall)/float(len(L_X_ID))
Y_homo_ratio = float(I_Y_homo)/float(len(L_Y_ID))
Y_hetero_ratio = float(I_Y_hetero)/float(len(L_Y_ID))
Y_NoCall_ratio = float(I_Y_NoCall)/float(len(L_Y_ID))
# Let's find XO
if X_hetero_ratio < 0.1 and Y_NoCall_ratio > 0.5 :
	outFile.write(sys.argv[1] +'\t'+ 'XO'+'\n')
# Let's find XXY
if X_hetero_ratio > 0.1 and Y_NoCall_ratio < 0.5 :
	outFile.write(sys.argv[1] +'\t'+ 'XXY'+'\n')
# Let's find XYY
if X_homo_ratio > 0.9 and X_hetero_ratio < 0.1 and Y_homo_ratio < 0.5 and Y_NoCall_ratio < 0.5 :
	outFile.write(sys.argv[1] +'\t'+ 'XYY'+'\n')
